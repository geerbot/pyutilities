#!/bin/bash

if modprobe --version > /dev/null; then
	sudo modprobe dummy
	sudo ip li add dummy0 type dummy
	sudo ip link set name eth10 dev dummy0
	sudo ip addr add 172.19.20.254 dev eth10
else
	echo "Don't have modprobe package"
fi

#sudo ip addr del 172.19.20.254/24 dev eth10
#sudo ip link delete eth10 type dummy
#sudo rmmod dummy
