from pysnmp import hlapi
import sys

g = hlapi.getCmd(hlapi.SnmpEngine(), hlapi.CommunityData(sys.argv[1], mpModel=0), hlapi.UdpTransportTarget((sys.argv[2], '161')), hlapi.ContextData(), hlapi.ObjectType(hlapi.ObjectIdentity(sys.argv[3])))

errIndication, errStatus, errIndex, vb = next(g)

if errStatus == 0:
    print(vb[0])
