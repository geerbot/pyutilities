import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--runfast", action="store_true", default=False, help="don't run slow tests"
    )


def pytest_collection_modifyitems(config, items):
    if not config.getoption("--runfast"):
        # --runslow given in cli: do not skip slow tests
        return
    skip_slow = pytest.mark.skip(reason="need --runfast option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)
