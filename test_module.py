import pytest, ConfigParser, os

def setup_module():
    pytest.config.pluginmanager.register(Reporter())

    if not pytest.config.getoption("--runfast"):
        # --runslow given in cli: do not skip slow tests
        return
    skip_slow = pytest.mark.skip(reason="need --runfast option to run")
    for item in items:
        if "slow" in item.keywords: item.add_marker(skip_slow) 

class Reporter():

    actual=0
    expected=0

    def __init__(self):
        self.fp=open('results.csv','w')
        self.fp.write('testCase,duration,result,entry config,acutal,expected,exit config\n')

    def pytest_runtest_logreport(self, report):
        if report.when == 'teardown':
            result='SKIPPED'
            if report.passed:
                result='PASSED'
            elif report.failed:
                result='FAILED'
            s=getattr(report, 'duration', 0.0)
            self.fp.write('%s,%.6f,%s,none,%d,%d,none\n' % (report.nodeid, s, result, self.actual, self.expected))

def setVals(act,exp):
    setattr(Reporter, 'actual', act)
    setattr(Reporter, 'expected', exp)

def test_func_fast():
    val =2 
    assert val == 2, 'Error'
    setVals(val,2)

@pytest.mark.slow
def test_func_slow():
    pass

