import zipfile
from lxml import etree
from shutil import copyfile

path='C:\\Users\\Travis\\Desktop\\sample.docx'
mod='C:\\Users\\Travis\\Desktop\\sample_mod.zip'
tree=None

class Item():
        def __init__(self):
            self.image=''
            self.date=''
            self.checksum=''

        def set_image(self,s):
            self.image=s

        def set_date(self,s):
            self.date=s
        
        def set_checksum(self,s):
            self.checksum=s

def checkForUpdate(node,text,item):
    if item.image == '':
        if text == 'dsp_image':
            print('try setting {}'.format(text))
            item.set_image(text)
    elif item.date == '':
        el=etree.Element('MyTest1')
        node.insert(1,el)
        item.set_date(text)
    elif item.checksum == '':
        item.set_checksum(text)
        print('found {} {} {}'.format(item.image, item.date, item.checksum))

def getChildren(node):
    return node.getchildren()

def iterChildren(node, item):
    for n in node:
        if n.tag[(n.tag.find('}')+1):] == 't':
            checkForUpdate(n,n.text,item)
        n = getChildren(n)
        iterChildren(n,item)

copyfile(path, mod)

with zipfile.ZipFile(mod) as zip:
    content=zip.read('word/document.xml')
    tree=etree.fromstring(content)
    el=etree.Element('MyTest')
    tree.insert(1,el)
    content=etree.tostring(tree)
    node=getChildren(tree)
    item=Item()
    #iterChildren(node,item)
    with open('sample_output.xml', 'w') as f:
        f.write(str(content))
