from junitparser import JUnitXml

xml = JUnitXml.fromfile('result.xml')

RR_SUITES = ( \
        'test_sample.TestClass', 1, \
        'test_sample.TestClass2', 2, \
        'test_sample.TestClass3', 3 \
        )
total=0
fail=0
RR=0
currTest=''
for test in xml:
    # New test suite
    if test.classname != currTest:
        if RR != 0:
            print'{} passes {} of {}'.format(RR, total-fail, total)
        fail=0
        total=0
        RR=0
        for i in range(0, len(RR_SUITES)/2):
            if test.classname == RR_SUITES[i*2]:
                currTest = test.classname
                RR=RR_SUITES[i*2+1]
    total += 1
    # Failed test
    if test.result:
        fail += 1
if RR != 0:
    print'{} passes {} of {}'.format(RR, total-fail, total)
