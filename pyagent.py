import sys
from pysnmp.entity import engine, config
from pysnmp.entity.rfc3413 import cmdrsp, context
from pysnmp.carrier.asyncore.dgram import udp
from pysnmp.proto.api import v2c
from time import sleep

OidClass=( \
        (1,3,6,1,4,1,41642,1,1,1,1,0), \
        (1,3,6,1,4,1,41642,1,1,1,2,0), \
        (1,3,6,1,4,1,41642,1,1,2,1,0), \
        )
OidLen=len(OidClass[0])
OidDelay=0

# Create SNMP engine
snmpEngine = engine.SnmpEngine()

# Transport setup

# UDP over IPv4
config.addTransport(
    snmpEngine,
    udp.domainName,
    udp.UdpTransport().openServerMode(('127.0.0.1', 161))
)

# SNMPv2c setup

# SecurityName <-> CommunityName mapping.
config.addV1System(snmpEngine, 'my-area', 'public')

# Allow read MIB access for this user / securityModels at VACM
config.addVacmUser(snmpEngine, 2, 'my-area', 'noAuthNoPriv', (1, 3, 6, 1, 4, 1, 41642))

# Create an SNMP context
snmpContext = context.SnmpContext(snmpEngine)

# --- create custom Managed Object Instance ---

mibBuilder = snmpContext.getMibInstrum().getMibBuilder()

MibScalar, MibScalarInstance = mibBuilder.importSymbols(
    'SNMPv2-SMI', 'MibScalar', 'MibScalarInstance'
)

class MyStaticMibScalarInstance(MibScalarInstance):
    # noinspection PyUnusedLocal,PyUnusedLocal
    def getValue(self, name, idx):
        sleep(OidDelay)
        if name == OidClass[0]:
            return self.getSyntax().clone(
                'Python %s running on a %s platform' % (sys.version, sys.platform)
            )
        elif name == OidClass[1]:
            return self.getSyntax().clone(3)
        elif name == OidClass[2]:
            return self.getSyntax().clone(5)

mibBuilder.exportSymbols(
    '__MY_MIB', MibScalar(OidClass[0][:OidLen-1], v2c.OctetString()),
    '__MY_MIB', MibScalar(OidClass[1][:OidLen-1], v2c.Integer()),
    '__MY_MIB', MibScalar(OidClass[2][:OidLen-1], v2c.Integer()),
    MyStaticMibScalarInstance(OidClass[0][:OidLen-1], (OidClass[0][OidLen-1],), v2c.OctetString()),
    MyStaticMibScalarInstance(OidClass[1][:OidLen-1], (OidClass[1][OidLen-1],), v2c.Integer()),
    MyStaticMibScalarInstance(OidClass[2][:OidLen-1], (OidClass[2][OidLen-1],), v2c.Integer())
)

# --- end of Managed Object Instance initialization ----

# Register SNMP Applications at the SNMP engine for particular SNMP context
cmdrsp.GetCommandResponder(snmpEngine, snmpContext)
cmdrsp.NextCommandResponder(snmpEngine, snmpContext)
cmdrsp.BulkCommandResponder(snmpEngine, snmpContext)

# Register an imaginary never-ending job to keep I/O dispatcher running forever
snmpEngine.transportDispatcher.jobStarted(1)

# Run I/O dispatcher which would receive queries and send responses
try:
    snmpEngine.transportDispatcher.runDispatcher()
except:
    snmpEngine.transportDispatcher.closeDispatcher()
    raise
