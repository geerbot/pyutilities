#!/bin/bash

function set_oids() {
	local arr
	arr=( 'sysDescr.0' '1.3.6.1.2.1.1.0' 1 65535 )
	declare -p arr
}

function get() {
	/usr/bin/snmpget -v2c -c $1 $2 $3
}

function set() {
	/usr/bin/snmpget -v2c -c $1 $2 $3
}

community=$1
ip=$2

oids=$(set_oids)
print ${oids[0]}
#for (( i = 0; i < ${#oids}/4; ++i )); do
#	print $oids[0]
#	get $community $ip $oids[1]
#	#print $val
#done
